#!/usr/bin/env node

import fetch from "node-fetch";
const APIURI = "https://fi.jamix.cloud/apps/menuservice/rest/haku/menu/92225/1?lang=fi";
const weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

function getAllFoods(data) {
  // get the list of days and sort them by date ascending (the api for some reason sorts by weekday)
  const days = data[0].menuTypes[0].menus[0].days.sort((a, b) => {
    return a.date - b.date;
  });

  const foods = [];
  days.map((day) => {
    foods.push(`${weekday[day.weekday]}, ${String(day.date)}`);
    day.mealoptions.map((mealoption, index) => {
      let str = ""
      str += mealoption.name + ": "
      mealoption.menuItems.map((menuItem, index) => {
        index !== mealoption.menuItems.length - 1 ? str += menuItem.name + ", " : str += menuItem.name;
      })
      foods.push(str);
      index === day.mealoptions.length - 1 ? foods.push("\n") : '';
    })
  })

  return foods;
}

fetch(APIURI)
  .then(response => response.json())
  .then(data => getAllFoods(data).map(food => console.log(food)))
  .catch(() => console.error("Error when fetching resource (api endpoint might be down)"));
